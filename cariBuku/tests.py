from django.test import TestCase, Client
from django.urls import resolve
from .views import index

class Lab8UnitTests(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        invalid_response = Client().get('/this_should_not_exist')
        self.assertEqual(invalid_response.status_code, 404)
    
    def test_lab8_using_index_function(self):
        foundFunc = resolve('/')
        self.assertEqual(foundFunc.func, index)
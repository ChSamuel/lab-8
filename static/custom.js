$(document).ready(function() {
    $('#searchButton').click(function(event) {
        search();
    })
})

function search(){
    var searchQuery = document.getElementsByTagName("input")[0].value;
    var fixedSearchQuery = "";
    for(var i = 0; i < searchQuery.length; i++){
        if(searchQuery.charAt(i) != " "){
            fixedSearchQuery = fixedSearchQuery + searchQuery.charAt(i);
        }
    }
    var searchURL = "https://www.googleapis.com/books/v1/volumes?q=" + fixedSearchQuery;

    var appendToHTML = "";
    
    $.ajax({
        url : searchURL,
        success : function(data){
            $("#output").empty();
            if(!data.items){
                appendToHTML += "Tidak ada buku dengan keyword tersebut";
            }
            else{
                appendToHTML += "<table class='table table-responsive table-striped'><tr><th>Judul Buku</th>";
                appendToHTML += "<th>Tahun Terbit</th><th>Pengarang</th><th>Thumbnail Buku</th></tr>";
                $.each(data.items, function(i, item){
                    appendToHTML += "<tr><td id='judulBuku'>"+item.volumeInfo.title+"</td>";
                    if(item.volumeInfo.publishedDate){
                        appendToHTML = appendToHTML+ "<td>" + item.volumeInfo.publishedDate + "</td>";
                    }
                    else{
                        appendToHTML += "<td>Tidak ada informasi</td>";
                    }
                    if(item.volumeInfo.imageLinks){
                        if(!item.volumeInfo.imageLinks.smallThumbnail){
                            if(!item.volumeInfo.imageLinks.thumbnail){
                                appendToHTML += "<td>Tidak ada preview thumbnail</td>";
                            }
                            appendToHTML += "<td><img src=" + item.volumeInfo.imageLinks.thumbnail + "></td>";
                        }
                        else{
                            appendToHTML += "<td><img src=" + item.volumeInfo.imageLinks.smallThumbnail + "></td>";
                        }
                    }
                    else{
                        appendToHTML += "<td>Tidak ada preview thumbnail</td>";
                    }

                    if(!item.volumeInfo.authors){
                        appendToHTML += "<td>Tidak ada informasi</td>";
                    }
                    else{
                        appendToHTML += "<td>";
                        $.each(item.volumeInfo.authors, function(j, author){
                            appendToHTML += author + "<br>";
                        });
                        appendToHTML += "</td>";
                    }
                });
            }
            appendToHTML += "</tr></table>"
            $("#output").append(appendToHTML);
        },
    });
}